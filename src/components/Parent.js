import React, {Component} from "react";
import Count from './Count';

class Parent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            childDisplay: false
        }
    }

    onBtnHandle = () => {
        this.setState({
            childDisplay: true 
        }) 
    }

    render() {
        return(
            <div >
                {this.state.childDisplay ? <Count display={this.onBtnHandle}/>: <button className="btn btn-info" onClick={this.onBtnHandle}>Create Element</button> } 
            </div>
        )
    }
}

export default Parent;