import React, {Component} from "react";
import "bootstrap/dist/css/bootstrap.min.css";
class Count extends Component {
    constructor(props) {
        super(props);

        this.state = {
            text: "Exit",
            count: 1
        }
    }

    addCounter = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    componentWillMount() {
        console.log('Component Will Mount');
    }

    componentDidMount() {
        console.log('Component Did Mount');
        // setInterval(() => this.addCounter(), 1000); //truyền vào callback function
    }

    componentWillUnmount() {
        console.log("Component will unmount");
    }
    componentDidUpdate() {
        console.log("Component Did Update");

        // if(this.state.count === 5) { //khi count = 10 xóa component đi
        //     this.state.display();
        // }
    }
    onBtnCloseElement = () => {
        this.setState ( {
            text: ""
        })
    }
    render() {
        return(
            <div>
            <button className="btn btn-warning" onClick={this.onBtnCloseElement}>Close Element</button>
            <p>{this.state.text}</p>
            </div>
        )
    }
}

export default Count;